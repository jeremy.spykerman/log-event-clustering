from setuptools import setup, find_packages

setup(name='log_event_classifier',
      version=0.1,
      description='Log message classification demonstration',
      url='https://gitlab.com/jeremy.spykerman/log-event-clustering',
      author='Jeremy Spykerman',
      author_email='jeremy.spykerman@gmail.com',
      license='MIT',
      packages=find_packages(),
      install_requires=[
          'textdistance[extras]',
          'python-dateutil'
      ],
      python_requires='>=3.7',
      tests_require=['nose'],
      test_suite='nose.collector',
      zip_safe=True)