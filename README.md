# Log Event Clustering

Sometimes it is useful to group log events into the same type of messages for analysis purposes.
There are plenty of commercial products that do this and some academic papers about it, but
I have yet to see a simple explanation of a way to do this automatic log clustering.

At a very basic level, this task requires grouping similar text strings together. How can you
measure the similarity of two strings?
[Levenshtein distance](https://en.wikipedia.org/wiki/Levenshtein_distance) is a way to measure
how different any two strings of text are.

Levenshtein distance is only part of the solution. The problem with using
this distance measure directly can be easily illustrated. The Levenshtein distance between `mitten` and `kitten` is 1. The distance between `I` and `a` is also 1. It is easy to see that the degree of difference in those two cases is
not equal. To compensate for this the Levenshtein distance can be scaled by dividing by the number of characters
in one of the strings. Now that we have a scaled value, we can consider two strings to be of the same class if
the scaled distance is less than a threshold.

If the threshold is 0.2, then `distance('mitten', 'kitten') / length('mitten')` is 0.16666... so it is below the threshold and that would mean `mitten` is in the same class as `kitten`.

This algorithm is much more interesting with actual log files. Enjoy.