'''
Automatic log event clustering script
'''

import argparse
import csv
from dateutil import parser
from log_event_classifier.classifier import LogClassifier

DATE_LENGTH = 16

def get_args():
    '''
    Create arg parser and return args
    '''
    arg_parser = argparse.ArgumentParser(
        description='automatic log event clustering demo')
    arg_parser.add_argument('input_log', type=str)
    arg_parser.add_argument('output_csv', type=str)
    return arg_parser.parse_args()


def parse_log(log_file_name):
    '''
    Generator that parses the log file. Log entries are generated as dictionaries.
    '''
    with open(log_file_name) as log_file:
        line = next(log_file, None)
        while line is not None:
            date_string = line[0:DATE_LENGTH]
            date = parser.parse(date_string)
            line = line[DATE_LENGTH:]
            line = line.replace(date_string, '').lstrip()
            message_point = line.find(': ')
            if message_point > 0:
                line = line[message_point + 2:]
            event = line
            if len(event) > 0:
                yield {'timestamp': date, 'message': event.strip()}
            line = next(log_file, None)

def write_classifications(parsed, outfile):
    '''
    Classify the parsed logs and write them as .csv to outfile
    '''
    classifier = LogClassifier()
    with open(outfile, 'w', newline='') as out_csv:
        writer = csv.DictWriter(out_csv, fieldnames=['timestamp', 'class', 'message'])
        writer.writeheader()
        for event in parsed:
            index = classifier.classify(event['message'])
            record = {'timestamp': event['timestamp'], 'class': index, 'message': event['message']}
            writer.writerow(record)


def main():
    '''
    Entry point function
    '''
    args = get_args()
    parsed = parse_log(args.input_log)
    write_classifications(parsed, args.output_csv)

if __name__ == '__main__':
    main()
