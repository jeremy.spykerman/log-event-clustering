import unittest
from log_event_classifier.classifier import LogClassifier

class TestLogClassifier(unittest.TestCase):

    def test_similar_messages(self):
        message1 = 'This is some message'
        message2 = 'These are some messages'

        classifier = LogClassifier()

        class1 = classifier.classify(message1)
        class2 = classifier.classify(message2)

        self.assertEqual(class1, class1)


    def test_very_different_messages(self):
        message1 = 'This is the first message'
        message2 = 'The quick brown fox jumped over the lazy dogs'

        classifier = LogClassifier()

        class1 = classifier.classify(message1)
        class2 = classifier.classify(message2)

        self.assertNotEqual(class1, class2)

if __name__ == '__main__':
    unittest.main()