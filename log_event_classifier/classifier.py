'''
Module for clustering log messages into prototype classes
'''

import textdistance


class LogClassifier:
    '''
    LogClassifier clusters log messages based on similarity to previously seen messages
    '''

    def __init__(self, threshold=0.1):
        super().__init__()
        self.__threshold = threshold
        self.__prototypes = list()

    def classify(self, message):
        '''
        Given a message string, find either a matching class or create a new prototype message.
        Return the index of the prototype.
        '''
        found_class = False
        for index, class_example in enumerate(self.__prototypes):
            distance = textdistance.levenshtein(class_example, message)
            ratio = distance / len(message)
            if ratio < self.__threshold:
                found_class = index
                break
        if found_class is False:
            index = len(self.__prototypes)
            self.__prototypes.append(message)
        return index

    def get_prototypes(self):
        '''
        Get the list of prototype messages for each classification
        '''
        return self.__prototypes
